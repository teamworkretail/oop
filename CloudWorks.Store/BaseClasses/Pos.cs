﻿using CloudWorks.Store.Classes;
using CloudWorks.Store.Interfaces;
using System;
using System.Collections.Generic;

namespace CloudWorks.Store.BaseClasses
{
    public abstract class Pos : IPrintable
    {
        protected Pos(int posNumber)
        {
            PosNumber = posNumber;
            this.History = new List<KeyValuePair<DateTime, Client>>();
        }

        public int PosNumber { get; }

        //Для історії потрібні часові відмітки, яких немає в класі клієнт, тому було б зручно щоб елементом колекції
        //була пара а не тільки екземпляр класу - це позбавить необхідності створення окремого класу для елемента історії
        //Доступ по точній часовій відмітці малоімовірний, можливо буде потрібний пошук елементів за відрізок часу
        //Dictionary - реалізований на основы хеш таблиць, швидкий доступ до елемента по ключу
        //List vs LinkedList - підходять одинаково так як різниця є тільки в доступі по індексу
        //і додаванні елемента в середину списку. Ніодної з цих операцій не передбачено, тому підійде будь-яка колекція
        //висновок так як в Dictionary більш складна структура можливі більші затрати памяті. Тому використаємо List
        public List<KeyValuePair<DateTime, Client>> History { get; private set; }

        public virtual double Checkout(Client client)
        {
            this.History.Add(new KeyValuePair<DateTime, Client>(DateTime.Now, client));
            return default(double);
        }

        public abstract void Print();
    }
}
